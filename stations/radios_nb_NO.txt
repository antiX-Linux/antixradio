# NRK Norsk rikskringkasting. Quelle: https://lyd.nrk.no/
# For details about this station see: https://no.wikipedia.org/wiki/NRK
# last updated 12/2023
NRK Alltid Nyheter,https://lyd.nrk.no/nrk_radio_alltid_nyheter_aac_h.m3u
# also https://lyd.nrk.no/nrk_radio_alltid_nyheter_mp3_h.m3u
NRK Folkemusikk,https://lyd.nrk.no/nrk_radio_folkemusikk_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_folkemusikk_mp3_h.m3u
NRK jazz,https://lyd.nrk.no/nrk_radio_jazz_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_jazz_mp3_h.m3u
NRK Klassisk,https://lyd.nrk.no/nrk_radio_klassisk_aac_h.m3u
#also: https://lyd.nrk.no/nrk_radio_klassisk_mp3_h.m3u
NRK mP3,https://lyd.nrk.no/nrk_radio_mp3_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_mp3_mp3_h.m3u
NRK P13,https://lyd.nrk.no/nrk_radio_p13_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p13_mp3_h.m3u
NRK P1 Buskerud,https://lyd.nrk.no/nrk_radio_p1_buskerud_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_buskerud_mp3_h.m3u
NRK P1 Finnmark,https://lyd.nrk.no/nrk_radio_p1_finnmark_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_finnmark_mp3_h.m3u
NRK P1 Hedmark og Oppland,https://lyd.nrk.no/nrk_radio_p1_hedmark_og_oppland_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_hedmark_og_oppland_mp3_h.m3u
NRK P1 Hordaland,https://lyd.nrk.no/nrk_radio_p1_hordaland_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_hordaland_mp3_h.m3u
NRK P1 Innlandet,https://lyd.nrk.no/nrk_radio_p1_innlandet_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_innlandet_mp3_h.m3u
NRK P1 Møre og Romsdal,https://lyd.nrk.no/nrk_radio_p1_more_og_romsdal_aac_h.m3u
# also https://lyd.nrk.no/nrk_radio_p1_more_og_romsdal_mp3_h.m3u
NRK P1 Nordland,https://lyd.nrk.no/nrk_radio_p1_nordland_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_nordland_mp3_h.m3u
NRK P1 Østfold,https://lyd.nrk.no/nrk_radio_p1_ostfold_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_ostfold_mp3_h.m3u
NRK P1 Østlandssendingen,https://lyd.nrk.no/nrk_radio_p1_ostlandssendingen_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_ostlandssendingen_mp3_h.m3u
NRK P1 Rogaland,https://lyd.nrk.no/nrk_radio_p1_rogaland_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_rogaland_mp3_h.m3u
NRK P1 Sogn og Fjordane,https://lyd.nrk.no/nrk_radio_p1_sogn_og_fjordane_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_sogn_og_fjordane_mp3_h.m3u
NRK P1 Sørlandet,https://lyd.nrk.no/nrk_radio_p1_sorlandet_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_sorlandet_mp3_h.m3u
NRK P1 Stor-Oslo,https://lyd.nrk.no/nrk_radio_p1_stor-oslo_aac_l.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_stor-oslo_mp3_h.m3u
NRK P1 Telemark,https://lyd.nrk.no/nrk_radio_p1_telemark_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_telemark_mp3_h.m3u
NRK P1 Troms,https://lyd.nrk.no/nrk_radio_p1_troms_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_troms_mp3_h.m3u
NRK P1 Trøndelag,https://lyd.nrk.no/nrk_radio_p1_trondelag_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_trondelag_mp3_h.m3u
NRK P1 Vestfold,https://lyd.nrk.no/nrk_radio_p1_vestfold_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1_vestfold_mp3_h.m3u
NRK P1+,https://lyd.nrk.no/nrk_radio_p1pluss_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p1pluss_mp3_h.m3u
NRK P2,https://lyd.nrk.no/nrk_radio_p2_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p2_mp3_h.m3u
NRK P3,https://lyd.nrk.no/nrk_radio_p3_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p3_mp3_h.m3u
NRK Urørt,https://lyd.nrk.no/nrk_radio_p3_urort_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p3_urort_mp3_h.m3u
NRK P3X,https://lyd.nrk.no/nrk_radio_p3x_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_p3x_mp3_h.m3u
NRK Sport,https://lyd.nrk.no/nrk_radio_sport_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_sport_mp3_h.m3u
NRK Evenement 1,https://lyd.nrk.no/nrk_radio_weblyd01_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_weblyd01_mp3_h.m3u
NRK Evenement 2,https://lyd.nrk.no/nrk_radio_weblyd02_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_weblyd02_mp3_h.m3u
NRK Evenement 3,https://lyd.nrk.no/nrk_radio_weblyd03_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_weblyd03_mp3_h.m3u
NRK Evenement 4,https://lyd.nrk.no/nrk_radio_weblyd04_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_weblyd04_mp3_h.m3u
NRK Evenement 5,https://lyd.nrk.no/nrk_radio_weblyd05_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_weblyd05_mp3_h.m3u
NRK Sámi radio,https://lyd.nrk.no/nrk_sapmi_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_sami_mp3_h.m3u
NRK Super,https://lyd.nrk.no/nrk_super_aac_h.m3u
# also: https://lyd.nrk.no/nrk_radio_super_mp3_h.m3u
