# SRF Schweizerischer Telefonrundspruch, Quelle: https://www.srf.ch/website-und-apps-livestreams
# https://www.broadcast.ch/fileadmin/kundendaten/Dokumente/Internet_Streaming/2022_07_links_for_streaming_internetradio_de_fr_it.pdf
# last updated 12/2023
RTS 1 La Première,https://stream.srg-ssr.ch/la-1ere/mp3_128.m3u
RTS 2 Espace,https://stream.srg-ssr.ch/espace-2/mp3_128.m3u
RTS 3 Coleur,https://stream.srg-ssr.ch/couleur3/mp3_128.m3u
RTS Option Musique,https://stream.srg-ssr.ch/option-musique/mp3_128.m3u
RTS Classique,https://stream.srg-ssr.ch/rsc_fr/mp3_128.m3u
RTS Jazz,https://stream.srg-ssr.ch/rsj/mp3_128.m3u
RTS Pop,https://stream.srg-ssr.ch/rsp/mp3_128.m3u

