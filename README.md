# antiXradio

A tiny bash script providing a GUI for listening and recording radio from public non-drm FTA radio streams.

## Installation

Please use the debian style installer .deb package. The available package installs on 32 bit and 64 bit both.
It will take care of all dependencies and make antiXradio available from antiX main menu's program submenu.
After download please check the integrity of the package, e.g. by
```
$ shasum -c antixradio.deb.sha512.sum
OK
```
This must read OK, otherwise download was damaged or corrupted. In this case redownload.

If file was checked OK, installation can be done from the locally stored installer file e.g. by apt:
```
sudo apt-get update
sudo apt-get install ./antixradio.deb
```
Check for any errors in the console output of the installer.

## Manual installation (not reccomended)

Only in case you didn't install from a .deb package, follow the instructions below to put manually in place all the files needed:
- [ ] install all dependecies listed in script header: yad mpv socat nc (e.g. netcat-traditional) sed papirus-mini-antix
- [ ] copy file ./bin/miniradio → /usr/local/bin (make it executable)
- [ ] copy folder ./resources/stations → /usr/local/lib/antiXradio
- [ ] copy file ./resources/applications/antiXradio.desktop → /usr/share/applications (add your missing locale)
- [ ] copy folder ./resources/icons → /usr/local/lib/antiXradio
- [ ] register all icons as named icons e.g.
```
for i in 24 32 48 64 96 128 192 256; do
   sudo xdg-icon-resource install --novendor --mode system --size $i '/usr/local/lib/antiXradio/icons/'$i'x'$i'/antiXradio.png'
   sudo xdg-icon-resource install --novendor --mode system --size $i '/usr/local/lib/antiXradio/icons/'$i'x'$i'/antiXradio2.png'
   sudo xdg-icon-resource install --novendor --mode system --size $i '....
   ....
done
```
- [ ] copy folder ./resources/translations/locale → /usr/share/locale (not the .pot and .po files, but .mo files only!)
- [ ] refresh menu e.g by applying _desktop-menu --write-out-global_ (no sudo!)

## Usage

Start antiXradio from the menu entry in Programs/Multimedia subfolder of antiX main menu.

Check the tooltips available on it's buttons to learn how to use it.

Please note: You can only “power off” antiXradio by clicking it's dedicated “power off” button at left bottom line of it's main window. Using the X from upper window border or ESC key will cause antiXradio to run in background like a headless daemon while hiding it's GUI, so you can listen to the radio reception still without need of having it's window around on your workspace. To restore the main window later just call antiXradio again from it's menu entry.

To see Icy-Title information transferred by most radio stations check the tickbox "Show Icy-Titles" from the main window. These will be displayed in a tiny extra window, persistent even when you close the main window of antiXradio. Spoiler: Some stations don't transfer these pieces of information at all, or use a format not compatible with antiXradio. In these cases either the additional window will not come up, or it will display no useful content or stay empty. This can't be helped, please just close the icy-title info-window in these (hopefully rare) cases.

WARNING: When running antiXradio on windowmanagers without status bar there will be possibly only in the main window upper border an indication when you are recording. If you close this window while still listening, forgetting about the recording, your disk may run out of space unnoticed.

- [ ] You can deactivate the autostart functionality by manually editing it's config file ~/.config/antiXradio/antiXradio.conf. Change the entry _démarrage="true"_ to _démarrage="false"_ (case sensitive).
- [ ] You can set the width of icy-title info-window in settings file by writing the desired width in px to the largeur entry, e.g. _largeur="600"_
- [ ] You can edit the stations lists to add or remove entries to meet your preferences. Just make sure to keep the format found in them. Comments on new lines and at end of lines allowed, these must start with a sharp sign #. Also empty lines between sets of entries in a list allowed.

The configuration file will be created automatically on first run.

antiXradio can receive only non-drm (non-encrypted) FTA radio streams without subscription, it will never break any kind of drm protection. Also it doesn't support login credentials for paid services.

## In case of problems running antiXradio

In case the script doesn't work as expected, please follow the following instructions:

If antiXradio refuses to come up at all, please check for the presence of a lockfile named _antiXradio_running_ in your _/tmp_ folder, and if present remove it. Make sure beforehand antiXradio isn't actually running in the background by checking the process list e.g. using htop.

For all other issues please start antiXradio from within a console window (e.g. roxterm) and check for error messages displayed.

For a more detailed tracing, open script file first and set _flag_debugmode=0_ found in line 43 to _flag_debugmode=1_ and save the file. Then run antiXradio from within a terminal window (e.g. roxterm) to see all it's processing output, which should give at least a hint at which point processing fails.

If you can't make it run, please just ask in antiX forum for help, best would be to copy all the debug output you've got to a textfile and attach it to your posting.

## Questions, suggestions and bug reporting

Please file all your questions, suggestions and bug reporting to [antiX Forum](https://www.antixforum.com)

## Countributions

All kind of improvement of script code welcome. Just post your proposal in antiXforum so it can be discussed, and/or send a merge request here.

If you want to participate in translation to your language, please register (for free) at transifex in the role of translator.
You'll find the resource at [antiX-developement/antiXradio](https://app.transifex.com/anticapitalista/antix-development/antixradio/), and you can edit it either within transifex online translation editor or by downloading the respective language file for offline editing and reuploading to transifex after your edit was completed.

If you want to contribute by adding or improving a stations list file from your country, please attach this file to a posting in antiXforum. Please make sure only to include stations legitimate available to the public.

## Authors and acknowledgment

This script was written in 2023 by Ch'Pol, PPC, Wallon and Robin for antiX comunity.

## License
[GNU GPL version 3](https://www.gnu.org/licenses/gpl-3.0.html)
